-- 1 Создать таблицу клиента с полями:
-- ● Фамилия
-- ● Имя
-- ● Дата рождения
-- ● Телефон
-- ● Почта
-- ● Список названий книг из библиотеки

create table users
(
    id          serial primary key,
    userName    varchar(30) NOT NULL,
    userSurname varchar(30) not null,
    userBirthday timestamp  not null,
    userTelephone varchar(30) not null,
    userEmail varchar(30),
    userBooks varchar(100)
);