package com.example.dzspring;

import com.example.dzspring.dbtask.dao.BookDaoBean;
import com.example.dzspring.dbtask.dao.UserDaoBean;
import com.example.dzspring.dbtask.model.Book;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import static java.time.LocalDate.now;

@SpringBootApplication
public class DzSpringApplication
            implements CommandLineRunner {

    private UserDaoBean userDaoBean;
    private BookDaoBean bookDaoBean;

    public DzSpringApplication(UserDaoBean userDaoBean, BookDaoBean bookDaoBean) {
        this.userDaoBean = userDaoBean;
        this.bookDaoBean = bookDaoBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(DzSpringApplication.class, args);
    }

    @Override
    public void run(String... args) {

        try {

//            bookDaoBean.insertBook(
//                    "Путешествие из Петербурга в Москву",
//                    "А. Н. Радищев",
//                    now()
//            );
//            bookDaoBean.insertBook(
//                    "Недоросль",
//                    "Д. И. Фонвизин",
//                    now()
//            );
//           bookDaoBean.insertBook(
//                    "Доктор Живаго",
//                    "Б. Л. Пастернак",
//                    now()
//            );
//            bookDaoBean.insertBook(
//                    "Сестра моя - жизнь",
//                    "Б. Л. Пастернак",
//                    now()
//            );
//            bookDaoBean.insertBook(
//                    "Война и мир",
//                    "Л. Н. Толстой",
//                    now()
//            );


//            userDaoBean.insertUser("Victor", "Dudkin", java.sql.Date.valueOf("2013-09-04"),
//                    "8-800-001", "aaa@list.ru","Доктор Живаго,Война и мир");

//            userDaoBean.insertUser("Oleg", "Babkin", java.sql.Date.valueOf("2006-02-20"),
//                    "8-800-003", "bb@list.ru","Путешествие из Петербурга в Москву,Недоросль");
            List<Book> userBooks = userDaoBean.findUserBooksByTelephone("8-800-003");
            System.out.println(userBooks.toString());

        } catch (SQLException e) {
            System.out.println("Что-то пошло не так!" + e.getMessage());
        }




    }

}
