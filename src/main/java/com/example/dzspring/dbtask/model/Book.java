package com.example.dzspring.dbtask.model;

import lombok.*;

import java.util.Date;

/**
 * Модель класса Книга
 */
//POJO - Plain Old Java Object
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Book {
    @Setter(AccessLevel.NONE)
    //@Getter(AccessLevel.NONE)
    private Integer bookId;
    private String bookTitle;
    private String bookAuthor;
    private Date dateAdded;
}
