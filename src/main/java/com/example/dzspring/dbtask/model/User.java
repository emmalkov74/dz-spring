package com.example.dzspring.dbtask.model;

import lombok.*;
import java.util.Date;


/**
 * Модель класса пользователь
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    @Setter(AccessLevel.NONE)
    //@Getter(AccessLevel.NONE)
    private Integer userId;
    private String userName;
    private String userSurname;
    private Date userBirthday;
    private String userTelephone;
    private String userEmail;
    private String userBooks;
}
