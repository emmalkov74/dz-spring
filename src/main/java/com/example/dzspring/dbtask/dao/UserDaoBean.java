package com.example.dzspring.dbtask.dao;

import com.example.dzspring.dbtask.model.Book;
import com.example.dzspring.dbtask.model.User;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс объекта доступа данных к таблице пользователи
 */
@Component
public class UserDaoBean {

    //поле объекта соединения
    private final Connection connection;

    //поле объекта  доступа данных к таблице книги
    private BookDaoBean bookDaoBean;

    /**
     * Конструктор класса принимающий на вход объект текущего соединения и объекта объекта доступа данных к таблице книги
     * @param connection объект соединения
     * @param bookDaoBean объект доступа данных к таблице книги
     */
    public UserDaoBean(Connection connection, BookDaoBean bookDaoBean) {

        this.connection = connection;
        this.bookDaoBean = bookDaoBean;
    }

    /**
     * Метод принимающий на вход поля объекта класса из модели пользователь и вставляющий их в
     *  соответствующую таблицу базы
     * @param userName имя
     * @param userSurname фамилия
     * @param userBirthday день рожденья
     * @param userTelephone телефон
     * @param userEmail электронная почта
     * @param userBooks книги пользователя
     * @throws SQLException выбрасываемое исключение
     */
    public void insertUser(String userName, String userSurname, Date userBirthday,
                           String userTelephone, String userEmail, String userBooks)
            throws SQLException {

        PreparedStatement insertQuery = connection.prepareStatement(
                "insert into users(username, usersurname, userbirthday, usertelephone," +
                        "useremail, userbooks)\n" +
                        "values (?, ?, ?, ?, ?, ?)"
        );

        insertQuery.setString(1, userName);
        insertQuery.setString(2, userSurname);
        insertQuery.setDate(3, userBirthday);
        insertQuery.setString(4, userTelephone);
        insertQuery.setString(5, userEmail);
        insertQuery.setString(6, userBooks);
        insertQuery.executeQuery();
        System.out.println("Книга добавлена!");

    }

    /**
     * Метод, который принимает телефон, достает из
     * UserDAO список названий книг данного человека. С этим списком ходить в BookDAO и
     * получает всю информацию об этих книгах
     * @param telephone телефон
     * @return список книг пользователя
     * @throws SQLException выбрасываемое исключение
     */
    public List<Book> findUserBooksByTelephone(String telephone) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement(
                "select * from users where usertelephone = ?");
        selectQuery.setString(1, telephone);
        ResultSet resultSet = selectQuery.executeQuery();
        String userBooksNames = "";
        while (resultSet.next()) {
            userBooksNames = resultSet.getString("userbooks");
        }
        String[] userBooksNamesArray = userBooksNames.split(",");
        List<Book> bookList = new ArrayList<>();
        for (String bookTitle: userBooksNamesArray) {
            bookTitle.trim();
            bookList.add(bookDaoBean.findBookByTitle(bookTitle));
        }
        return bookList;
    }

    
}
